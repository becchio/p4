# p4

Project 4

## Build

```
# docker stop $(docker ps -a -q); docker rm $(docker ps -aq); docker rmi $(docker images -q);
# cd docker

# docker stop $(docker ps -a -q); docker rm $(docker ps -aq) && \

docker stop $(docker ps -a -q); docker rm $(docker ps -aq); docker rmi $(docker images -q);
#docker stop $(docker ps -a -q); docker rm $(docker ps -aq);
docker build . && \
docker run -i $(echo -e $(docker images -q)|awk ' { print $1 } ') && \
container=$(docker ps -alq) && \
docker start $container && docker exec -it $container service mysql start && docker exec -it $container /bin/bash;

# docker exec -it $container service mysql start && docker exec -it $container mysql -u root < sql.init

```

- copy to remote

```
docker cp login.jsp $container:/app/examples/jsp/login/
```

- Docker tag

```
docker login
docker tag $img_id gbecchio/p4:version${n_n}
docker push gbecchio/p4:version${n_n}
```

- Testing Tools

  -  testinfra
For testing the infrastructure.

    - Installation

```
apt install python-virtualenv sshpassdocker stop $(docker ps -a -q); docker rm $(docker ps -aq); docker rmi $(docker images -q);

virtualenv test_infra

source test_infra/bin/activate

pip install testinfra
```

    - Use

```
vim test_infra_p4.py

py.test -v test_infra_p4.py

```

  - Sonarqube
    - Installation

```
cd sonar

mkdir -p sonarqube_conf;
mkdir -p sonarqube_extensions;
mkdir -p sonarqube_logs;
mkdir -p sonarqube_data;

docker stop $(docker ps -a -q); docker rm $(docker ps -aq);

jdbc_user=# XXX
jdbc_password=# XXX

p4_path=$HOME/p4/sonar

docker run -d --name sonarqube_p4_db \
  -p 9000:9000 \
  -e sonar.jdbc.username=$jdbc_user \
  -e sonar.jdbc.password=$jdbc_password \
  -v $p4_path/sonarqube_conf:/opt/sonarqube/conf \
  -v $p4_path/sonarqube_extensions:/opt/sonarqube/extensions \
  -v $p4_path/sonarqube_logs:/opt/sonarqube/logs \
  -v $p4_path/sonarqube_data:/opt/sonarqube/data \
  sonarqube

docker run -d --name sonarqube_p4 \
  -p 9000:9000 \
  -v $p4_path/sonarqube_conf:/opt/sonarqube/conf \
  -v $p4_path/sonarqube_extensions:/opt/sonarqube/extensions \
  -v $p4_path/sonarqube_logs:/opt/sonarqube/logs \
  -v $p4_path/sonarqube_data:/opt/sonarqube/data \
  sonarqube

docker run -d --name sonarqube_p4 -p 9000:9000 sonarqube
```

/etc/gitlab-runner/config.toml
```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  shell = "bash"
  name = "CI"
  url = "https://framagit.org/"
  token = "fCQVjNkxZWTziQohcQH_"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "ruby:2.6"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    network_mode="p4"
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]

```