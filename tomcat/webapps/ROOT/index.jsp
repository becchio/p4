<%@page import="java.sql.*, javax.sql.*, java.io.*, javax.naming.*"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Connexion à A vos Marques</title>
</head>
<body>
<form action="index.jsp" method="get">
Login: <input type="text" id="identifiant" name="identifiant">
<br/>
mot de passe: <input type="password" id="motDePasse" name="motDePasse">
<br/>
<input type="submit" id="go">
</form>

<%

    // Obtain our environment naming context
    Context initCtx = new InitialContext();
    DataSource ds = (DataSource) initCtx.lookup("java:comp/env/jdbc/TestDb");
    Connection con = ds.getConnection();
try
{

    // Allocate and use a connection from the pool
    String identifiant = request.getParameter("identifiant");
    String motDePasse = request.getParameter("motDePasse");

    String query = "select * from utilisateurs where ident=? and pass=? limit 0,1";
    
    PreparedStatement stmt = con.prepareStatement(query);
   
    stmt.setString(1, identifiant);
    stmt.setString(2, motDePasse);

    ResultSet rs = stmt.executeQuery();
    if(rs.next())
    {
        out.println("Vous êtes bien connecté "+identifiant);
    }
    else
    {
        out.println("Erreur d'authentification pour"+identifiant);
    }
    con.close();
}
catch(SQLException e)
{
    e.printStackTrace();
}
catch(Exception e)
{
    e.printStackTrace();
}
%>
</body>
</html>